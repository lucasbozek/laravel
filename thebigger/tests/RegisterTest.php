<?php


class RegisterTest extends TestCase
{
    public function testInit()
    {
        $this->assertEquals(true, 1 + 1 == 3);
    }

    public function testGetRequest()
    {
        $response = $this->call('GET', '/register');

        $this->assertEquals(200, $response->status());
    }

    public function testRegisterPageAccess()
    {
        $this->visit('register')->seeElement('#registerForm');
    }

    public function testWrongRegisterUser()
    {
        $this->visit('register')
            ->type('name', 'email')
            ->type('test', 'password')
            ->type('test', 'password_confirm')
            ->press('register')
            ->seePageIs('register');

    }

    public function testSuccessRegisterUser()
    {
        $this->visit('/register')
            ->type('naffe@go2.pl', 'email')
            ->type('test', 'password')
            ->type('test', 'password_confirm')
            ->press('register')
            ->seePageIs('/welcome');
    }
}