@extends('layout.layout')

@section('mainContent')
    <div style="min-height:700px">
        <main id="registerForm" class="container z-depth-1 mainForm">
            <div class="row center mainPolygon">
                <h1><span class='mainPolygonText'>register</span> </h1>
            </div>

            <div class="row">
                {{ Form::open(array('id' => 'register_form')) }}
                <div class="row">
                    <div class="input-field col l6 m12 s12 offset-l3">

                        <i class="material-icons prefix registerIcon">email</i>

                        <input name="email" id="email" type="text" class="validate">
                        <label for="email">Email</label>

                        <p class="errorMessage">{{ $errors->first('email') }}</p>

                    </div>
                </div>
                <div class="row">
                    <div class="input-field col l6 s12 offset-l3">

                        <i class="material-icons prefix registerIcon">vpn_key</i>

                        <input name="password" id="password" type="password">
                        <label for="email">Password</label>

                        <p class="errorMessage">{{ $errors->first('password') }}</p>

                    </div>
                </div>
                <div class="row">
                    <div class="input-field col l6 s12 offset-l3">

                        <i class="material-icons prefix registerIcon">loop</i>

                        <input name="password_confirm" id="password_confirm" type="password">
                        <label for="password_confirm">Confirm Password</label>

                        <p class="errorMessage">{{ $errors->first('password_confirm') }}</p>

                    </div>
                </div>

                <div class="row center">
                    <div>
                        {{ Form::submit('register', array('id' =>'register', 'class' => 'btn', 'style' => 'background:rgba(230, 126, 34, 0.78)!important')) }}
                        {{ Form::reset('Reset', array('id' => 'reset_button', 'class' => 'btn', 'style' => 'background:rgba(230, 126, 34, 0.78)!important')) }}
                    </div>
                </div>

                {{ Form::close() }}

            </div>
        </main>

    </div>

@endsection