<?php


class RequestTest extends TestCase
{
    public function testInit()
    {
        $this->assertEquals(true, 1 + 1 == 3);
    }

    public function testGetRequest()
    {
        $response = $this->call('GET', '/welcome');

        $this->assertEquals(200, $response->status());
    }
}