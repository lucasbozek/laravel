@extends('layout.layout')

@section('mainContent')
    <div style="min-height:700px">
        <main id="loginForm" class="container z-depth-1 mainForm">
            <div class="row center mainPolygon">
                <h1><span class='mainPolygonText'>sign in</span> </h1>
            </div>

            <div class="row">
                {{ Form::open(array('id' => 'loginForm', 'class' => 'form-horizontal')) }}
                <div class="row">
                    <div class="input-field col l6 m12 s12 offset-l3 {{ $errors->has('email') ? ' has-error' : null }}">

                        <i class="material-icons prefix registerIcon">email</i>

                        <input name="email" id="email" type="text" class="validate">
                        <label for="email">Email</label>

                        <p class="help-block">{{ $errors->first('email') }}</p>

                    </div>
                </div>
                <div class="row">
                    <div class="input-field col l6 s12 offset-l3 {{ $errors->has('password') ? ' has-error' : null }}">

                        <i class="material-icons prefix registerIcon">vpn_key</i>

                        <input name="password" id="password" type="password">
                        <label for="email">Password</label>

                        <p class="help-block">{{ $errors->first('password') }}</p>

                    </div>
                </div>

                <div class="row center">
                    <div>
                        {{ Form::submit('login', array('class' => 'btn', 'style' => 'background:rgba(230, 126, 34, 0.78)!important')) }}

                    </div>
                </div>

                {{ Form::close() }}

            </div>
        </main>

    </div>

@endsection