<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Input;
use Validator;
use Redirect;
use Sentinel;
use Hash;

/**
 * Controller handles the registration of new users, authenticating users for the application and
 * redirecting as well as allows users to reset their passwords.
 *
 * Class AuthController
 * @package App\Http\Controllers
 */
class AuthController extends Controller
{
    /**
     * Only guest user
     */
    const GUEST_ONLY = 0;

    /**
     * Only registred user
     */
    const REGISTRED_ONLY = 1;

    /**
     * Register page
     *
     * @Route /register
     * @Method GET
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function register()
    {
       return $this->getTemplateOrRedirect(__FUNCTION__, self::GUEST_ONLY);
    }

    /**
     * Register action
     *
     * @Route /register
     * @Method POST
     *
     * @return mixed
     */
    public function registerAction()
    {
        $inputs = Input::all();

        $rules = [
            'email' => 'required|email|unique:users',
            'password' => 'required',
            'password_confirm' => 'required|same:password',
        ];

        $validator = Validator::make($inputs, $rules);

        if($validator->fails()) {
            return Redirect::back()
                ->withInput()
                ->withErrors($validator);
        }

        if($user = Sentinel::registerAndActivate($inputs)) {
            return Redirect::to('/')
                ->withSuccess('Your account was successfully created. You might login now.')
                ->with('userId', $user->getUserId());
        }

        return Redirect::to('register')
            ->withInput()
            ->withErrors('Failed to register.');
    }

    /**
     * @Route /login
     * @Method Get
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
	public function login()
	{
        return $this->getTemplateOrRedirect(__FUNCTION__, self::GUEST_ONLY);
	}

    /**
     * @return mixed
     */
    public function loginAction()
    {
        $input = Input::all();

        $rules = [
            'email' => 'required|email',
            'password' => 'required',
        ];

        $validator = Validator::make($input, $rules);

        if($validator->fails()) {
            return Redirect::back()
                ->withInput()
                ->withErrors($validator);
        }

        $remember = (bool) Input::get('remember', false);

        if(Sentinel::authenticate(Input::all(), $remember)) {
            return Redirect::to('/')
                ->withSuccess('Successful login.');
        }

        $errors = 'Invalid login or password.';

        return Redirect::back()
            ->withInput()
            ->withErrors($errors);
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function resetPass()
    {
        return $this->getTemplateOrRedirect(__FUNCTION__, self::REGISTRED_ONLY);

    }

    /**
     * @return mixed
     */
    public function resetPassAction()
    {
        $input = Input::all();

        $rules = [
            'password' => 'required',
            'password_confirmation' => 'required|same:password',
        ];

        $validation = Validator::make($input,$rules);

        if($validation->fails()) {
            return Redirect::back()
                ->withInput()
                ->withErrors($validation);
        }

        $user = Sentinel::getUser();
        $user->password = Hash::make(Input::get('password', false));
        $user->save();

        return Redirect::to("/");
    }

    /**
     * Where to redirect user depends of credentials
     * @param $template template name
     * @param int $credentials
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    final private function getTemplateOrRedirect($template, $credentials = self::GUEST_ONLY)
    {
        $isLogged = ($credentials) ?
            Sentinel::getUser() : !Sentinel::getUser();

        if($isLogged) {
            return view('auth.'.$template);
        } else {
            return Redirect::to("/");
        }

    }

}