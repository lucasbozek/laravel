<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

/**
 * login
 */
Route::get('login', 'AuthController@login');
Route::post('login', 'AuthController@loginAction');

/**
 * logout
 */
Route::get('logout', function()
{
    Sentinel::logout();
    return Redirect::to('/');
});

/**
 * Register
 */
Route::get('register', 'AuthController@register');
Route::post('register', 'AuthController@registerAction');
Route::get('resetpass', 'AuthController@resetPass');
Route::post('resetpass', 'AuthController@resetPassAction');

/**
 * API routes
 */
Route::group(['namespace' => 'Api'], function() {
    Route::resource('getUser', 'UserController@getUser');
});


