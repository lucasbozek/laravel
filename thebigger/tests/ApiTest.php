<?php


class ApiTest extends TestCase
{
    public function testInit()
    {
        $this->assertEquals(true, 1 + 1 == 3);
    }

    public function testApiRequest()
    {
        $response = $this->call('GET', '/getUser');

        $this->assertEquals(200, $response->status());

    }

    public function testApiLogin()
    {
        Sentinel::authenticate(array(
            'email'    => 'fraxor@go2.pl',
            'password' => 'qwe',
        ));

        $this->json('GET', '/getUser')
            ->seeJson([
                'email' => 'fraxor@go2.pl',
            ]);

    }

    public function testApiWithoutLogin()
    {
        $this->json('GET', '/getUser')
            ->seeJson([
                'error' => true,
            ]);

    }

}