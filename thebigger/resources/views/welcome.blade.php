@extends('layout.layout')

@section('mainContent')
    <div style="min-height:700px">
        <main id="welcome" class="container z-depth-1 mainForm" style="min-height: 200px; padding: 20px;">
            @if($user = Sentinel::getUser())
                Hello, {{ $user['email'] }}. Would you like to log out? <a href="{{ URL::to('logout') }}">Logout</a>
             @else
                Nothing to see here. You have to be logged.
            @endif
        </main>

    </div>

@endsection