<?php


class LoginTest extends TestCase
{
    public function testInit()
    {
        $this->assertEquals(true, 1 + 1 == 3);
    }

    public function testGetRequest()
    {
        $response = $this->call('GET', '/login');

        $this->assertEquals(200, $response->status());
    }

    public function testLoginPage()
    {
        $this->visit('login')->seeElement('#loginForm');
    }

    public function testLogin()
    {
        Sentinel::authenticate(array(
            'email'    => 'fraxor@go2.pl',
            'password' => 'qwe',
        ));

        $this
            ->visit('/welcome')
            ->see('hello');
    }

    public function testLogout()
    {
        Sentinel::logout();

        $this
            ->visit('/welcome')
            ->see('Nothing to see here.');
    }

}