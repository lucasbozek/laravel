<?php

namespace App\Http\Controllers\Api;

use Response;
use Sentinel;

/**
 * Controller for user API action
 *
 * Class UserController
 * @package App\Http\Controllers\Api
 */
class UserController
{
    public function getUser()
    {
        $statusCode = 200;

        if($user = Sentinel::check()) {
            $response = $user;
        } else {
            $response = [
                'error' => true,
                'message' => 'You must be logged in'
            ];
        }

        return Response::json($response, $statusCode);
    }
}