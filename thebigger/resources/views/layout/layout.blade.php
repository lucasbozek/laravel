<html lang="en">
<head>  
	<meta charset="UTF-8">
    <title> TheBiggerApp </title>
	
	<!-- Style sheets -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.97.7/css/materialize.min.css" type="text/css">
    <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
	<link rel="stylesheet" href="css/style.css" type="text/css">
	
	<!-- Scripts -->
    <script src="https://code.jquery.com/jquery-3.1.1.min.js" integrity="sha256-hVVnYaiADRTO2PzUGmuLJr8BLUSjGIZsDYGmIJLv2b8=" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.97.7/js/materialize.min.js"></script>
</head>
<body style=" background-image: url({{asset('img/bg.png')}}); background-color: #765279">
	<header>
		<div class="row" style="background: #204d74">
			<nav style="background: #324456">
				<div class="nav-wrapper">
					<ul id="nav-mobile" class="right hide-on-med-and-down">
						@if ($user = Sentinel::check())
							<li><a href="{{ URL::to('logout') }}">Logout</a></li>
						@else
							<li><a href="{{ URL::to('login') }}">Login</a></li>
							<li><a href="{{ URL::to('register') }}">Register</a></li>
						@endif
					</ul>
				</div>
			</nav>
		</div>
	</header>

	<div class="row">
		@if ($errors->any())
			<div class="row center">
				<strong>Error</strong>
				@if ($message = $errors->first(0, ':message'))
					{{ $message }}
				@else
					Please check the form below for errors
				@endif
			</div>
		@endif

		@if ($message = Session::get('success'))
			<div class="row center">
				<strong>Success</strong> {{ $message }}
			</div>
		@endif
	</div>

@yield('mainContent')

	<footer class="page-footer footer">
		<div class="footer-copyright">
			<div class="container">
				© 2016 Łukasz Bożek
			</div>
		</div>
	</footer>

<script>
	$( "#reset_button" ).bind("onclick", function() {

		var form = $( "#register_form" );

		form.find(':text, :password').each(function(){
			$(this).val('');
		});

	});
</script>
</body>
</html>